from math import pi, sqrt

import mpmath
import numpy as np
from scipy import integrate


def Poletkin90(Rp, Rs, xb, yb, zb, theta, eta, tol=1e-14):
    a = Rs / Rp
    dx = xb / Rs
    dy = yb / Rs
    dz = zb / Rs

    integration1_result, _error = integrate.quad(lambda r: dL(r, a, dx, dy, dz, eta, 1), 1, -1)
    integration2_result, _error = integrate.quad(lambda r: dL(r, a, dx, dy, dz, eta, -1), -1, 1)

    val1 = 4e-7 * sqrt(Rs * Rp) * integration1_result
    val2 = 4e-7 * sqrt(Rs * Rp) * integration2_result

    M0 = val1 + val2
    return M0


def dL(r, a, x, y, z, et, sg):
    """

    :param r:
    :param a:
    :param x:
    :param y:
    :param z:
    :param et:
    :param sg:
    :return:
    """
    s = np.sqrt(x ** 2 + y ** 2)
    rho = np.sqrt(s ** 2 + 2 * r * (x * np.cos(et) + y * np.sin(et)) + r ** 2)
    if sg == 1:
        l = z + np.sqrt(1 - r ** 2)
    if sg == -1:
        l = z - np.sqrt(1 - r ** 2)

    kk = 4 * a * rho / ((a * rho + 1) ** 2 + a ** 2 * l ** 2)
    k = np.sqrt(kk)

    K, E = mpmath.ellipk(kk), mpmath.ellipe(kk)

    Psi = (1 - 0.5 * kk) * K - E

    t1 = np.sin(et) * (x + r * np.cos(et))
    t2 = np.cos(et) * (y + r * np.sin(et))

    s = (k * rho ** 1.5)
    if s == 0:
        # very big number indicating +infinity
        return 1000000000000000000000
    return (t1 - t2) * Psi / s
