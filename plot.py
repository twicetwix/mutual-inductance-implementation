from math import pi, cos, sin, tan

import matplotlib.pyplot as plt
import numpy as np


def draw_coils(Rp, Rs, xb, yb, zb, theta, eta, title='Example'):
    n_pts = 500
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.set_title(title)
    # ax.axis('equal') # not implemented in matplotlib for 3d =(
    pc = np.array([xb, yb, zb])

    points = np.arange(0, 2 * pi * (1 - 1 / n_pts), 2 * pi / n_pts)
    pp = Rp * np.array([np.cos(points), np.sin(points), np.zeros(shape=(len(points),))])
    ax.plot(pp[0], pp[1], pp[2], label='First coil')
    ax.legend()

    if theta == pi / 2 or theta == -pi / 2:
        psx = Rs * np.cos(points) * cos(eta)
        psy = Rs * np.cos(points) * sin(eta)
        psz = (pc[2] + Rs * np.sin(points))
    else:
        rs = cos(theta) / np.sqrt((np.sin(points - eta)) ** 2 + (cos(theta) * np.cos(points - eta)) ** 2)
        psx = Rs * rs * np.cos(points)
        psy = Rs * rs * np.sin(points)
        psz = (pc[2] + Rs * rs * np.sin(points - eta) * tan(theta))

    ps = [pc[0] + psx, pc[1] + psy, psz]
    ax.plot(ps[0], ps[1], ps[2], 'C1', label='Second coil')
    ax.legend()

    ax.plot(ps[0], ps[1], np.zeros(shape=len(ps[0])), 'C4', label='Second coil projection')
    ax.legend()

    centers = np.array([[0, xb, xb, 0], [0, yb, yb, 0], [0, zb, 0, 0]])
    ax.plot(centers[0], centers[1], centers[2], 'C3', label='Angle')
    ax.legend()

    plt.show()
