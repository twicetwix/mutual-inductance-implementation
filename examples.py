import unittest
from math import pi
from time import time

from PPoletkin import PPoletkin
from Poletkin import Poletkin


# noinspection PyPep8Naming
from plot import draw_coils


class BookExampleTestCase(unittest.TestCase):

    DEFAULT_PRECISION = 5

    def test_example_19(self):
        title = 'Example 19'
        Rp = 0.16
        Rs = 0.1
        xb = 0
        yb = 0.043301  # 4.3301cm
        zb = 0.175
        theta = 60 * pi / 180
        eta = 0 * pi / 180
        expected_result = 13.6113

        t1 = time()
        result = Poletkin(Rp, Rs, xb, yb, zb, theta, eta)
        result = result * 1e9
        print(f"{title} ({round(time() - t1, 4)}s):", result, expected_result)
        draw_coils(Rp, Rs, xb, yb, zb, theta, eta, title=title)
        self.assertAlmostEqual(expected_result, result, places=4)

    def test_example_20(self):
        title = 'Example 20'
        Rp = 0.4  # 40 cm
        Rs = 0.1  # 10cm
        xb = 0
        yb = 0.2  # 20cm
        zb = 0.1  # 10cm
        theta = 90 * pi / 180  # 90
        eta = 0 * pi / 180  # 0
        expected_result = -10.7272

        t1 = time()
        result = Poletkin(Rp, Rs, xb, yb, zb, theta, eta)
        result = result * 1e9
        print(f"{title} ({round(time() - t1, 4)}s):", result, expected_result)
        draw_coils(Rp, Rs, xb, yb, zb, theta, eta, title=title)
        self.assertAlmostEqual(expected_result, result, places=4)

    # noinspection DuplicatedCode
    def test_example_21(self):
        title = 'Example 21'
        Rp = 0.4  # from example 20
        Rs = 0.1  # from example 20
        xb = yb = zb = 0 # same points
        theta = 90 * pi / 180  # 90
        eta = 0 * pi / 180  # 0
        expected_result = 0.0

        t1 = time()
        result = Poletkin(Rp, Rs, xb, yb, zb, theta, eta)
        result = result * 1e9
        print(f"{title} ({round(time() - t1, 4)}s):", result, expected_result)
        draw_coils(Rp, Rs, xb, yb, zb, theta, eta, title=title)
        self.assertEqual(expected_result, result)

    def test_example_22(self):
        title = 'Example 22'
        Rp = 0.4
        Rs = 0.1
        xb = 0.1
        yb = 0.1
        zb = 0.0
        theta = 90.0000 * pi / 180
        eta = 0 * pi / 180
        expected_result = 0.0

        t1 = time()
        result = Poletkin(Rp, Rs, xb, yb, zb, theta, eta)
        result = result * 1e9
        print(f"{title} ({round(time() - t1, 4)}s):", result, expected_result)
        draw_coils(Rp, Rs, xb, yb, zb, theta, eta, title=title)
        self.assertEqual(expected_result, result)

    def test_example_23(self):
        title = 'Example 23'
        Rp = 0.1
        Rs = 0.1
        xb = 0
        yb = 0
        zb = 0.04
        theta = 15 * pi / 180
        eta = 0 * pi / 180
        expected_result = 153.3233

        t1 = time()
        result = PPoletkin(Rp, Rs, xb, yb, zb, theta, eta)
        result = result * 1e9
        print(f"{title} ({round(time() - t1, 4)}s):", result, expected_result)
        draw_coils(Rp, Rs, xb, yb, zb, theta, eta, title=title)
        self.assertAlmostEqual(expected_result, result, places=4)

if __name__ == '__main__':
    unittest.main()
