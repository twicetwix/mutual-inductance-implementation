from math import pi
from time import time

from Poletkin import Poletkin


# noinspection PyPep8Naming
# noinspection DuplicatedCode
from plot import draw_coils


if __name__ == '__main__':
    Rp = float(input("Enter Radius of first coil (example value: 0.16): "))
    Rs = float(input("Enter Radius of second coil (example value: 0.11): "))
    xb = float(input("Enter x coordinate of the center of the second coil (example value: 0): "))
    yb = float(input("Enter y coordinate of the center of the second coil (example value: 0.043301): "))
    zb = float(input("Enter z coordinate of the center of the second coil (example value: 0.175): "))
    theta = int(input("Enter theta radius (example 60): ")) * pi / 180
    eta = int(input("Enter eta radius (example 0): ")) * pi / 180

    t1 = time()
    result = Poletkin(Rp, Rs, xb, yb, zb, theta, eta)
    result = result * 1e9
    print(f"Elapsed time ({round(time() - t1, 4)}s):", result)
    draw_coils(Rp, Rs, xb, yb, zb, theta, eta, title="Custom example")
