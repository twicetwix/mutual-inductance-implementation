from math import pi, sqrt, cos, tan

import mpmath
import numpy as np
from scipy import integrate

from Poletkin90 import Poletkin90

# integration precision
mpmath.mp.dps = 50


def PPoletkin(Rp, Rs, xb, yb, zb, theta, eta, tol=1e-14) -> float:
    """
    Author: Alexander Trushin
    Initial matlab implementation Author: Kirill Poletkin
    Source article: https://www.researchgate.net/publication/331861230_Efficient_calculation_of_the_mutual_inductance_of_arbitrarily_oriented_circular_filaments_via_a_generalisation_of_the_Kalantarov-Zeitlin_method

    :param Rp: 1st coil radius
    :param Rs: 2nd coil radius
    :param xb: x coordinate of the center of second coil
    :param yb: y coordinate of the center of second coil
    :param zb: z coordinate of the center of second coil
    :param tol: absolute tolerance (default: 1e-10)
    :return: mutual inductance
    
    ########################################################
    Initial Matlab documentation
    ########################################################

    Function returns the mutual inductance between two circular filaments of radius Rp and Rs,
    whose centers are separated by coordinates xb,yb,zb, and angular position
    defined by the angule theta (tilting angle defined in an interval of 0<=theta<=360 degrees)
    and eta (rotating angle around  vertical axis  defined in an interval of  0<=theta<=360).
    The angular misalignment is defined in the same way as given by  Gover's notation).

    All dimensions must be in "meters" and angles in "radians"

    The units have been adapted to
     the S.I. system

    Programmed by Kirill Poletkin
    """
    dz = zb / Rp

    integration_result, _error = integrate.quad(lambda p: dL(p, dz, theta, eta), 0, 2 * pi)
    M0 = 4e-7 * Rp * integration_result
    return M0


# Integrand function
def dL(p, z, th, et):
    # Dimensionless functions
    r = 1  # dimensionless radius
    l = z + r * np.sin(p - et) * tan(th)

    kk = 4 * 1 / ((1 * 1 + 1) ** 2 + (1 ** 2) * (l ** 2))
    k = np.sqrt(kk)

    K, E = mpmath.ellipk(kk), mpmath.ellipe(kk)

    Psi = (1 - 0.5 * kk) * K - E
    f = Psi / k
    return f
