%Function returns the mutual inductance between 
% the primary circle having a rdius of Rp and its projection on a tilting plane. 
%The angular misalignment is given by angles theta and eta, while the
%linear misalignment is defined by the coordinate zb of the point 
%B crossing the tilted plane and the Z-axis.
%
%
%All dimensions must be in "meters" and angles in "radians"

% The units have been adapted to the S.I. system

%Programmed by Kirill Poletkin
%%
function M =  PPoletkin( Rp,zb,theta,eta,tol)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
%Mutual inductance of coil and its projection on a tilted plane 

if nargin==4,  tol=1e-14;
elseif nargin<3 || nargin >4,
   error('Wrong number of parameters in function call (IPoletkin.m)!');
end

dz=zb/Rp;



M=4e-7*Rp*integral(@(p)dL(p,dz,theta,eta),0,2*pi,'RelTol',0,'AbsTol',tol);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Integrand function  
function f=dL(p,z,th,et)
 % p is the angle of integration
 % a is the ratio of Rs/Rp
 % th is the theta and et is the eta
  
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %%%%%%% Dimensionless functions
    r=1; % dimensionless radius
    rho=1;
    l=z+r.*sin(p-et).*tan(th);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
 kk=4.*1./((1.*1+1).^2+1.^2.*l.^2); 
  k=sqrt(kk);
 
  [K,E]=ellipke(kk);    

  Psi=(1-0.5*kk).*K-E;
      
f=Psi./(k);
