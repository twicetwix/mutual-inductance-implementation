Folder contains Matlab codes that calculate the mutual inductance between two circular filaments 
of radii Rp and Rs, whose centers are separated by coordinates xb,yb,zb, and angular position
defined by angules theta (tilting angle defined in an interval  of 0<=theta<=360 degrees) 
and eta (rotating angle around  vertical axis in in an interval of  0<=eta<=360 degrees ). 
The angular misalignment is defined in the same way as given by  Gover's notation.


The function Poletkin.m refers to Poletkin's formula (16) and function Poletkin90.m to (20) 
in the article: Kirill V. Poletkin and Jan G. Korvink. Efficient calculation of the mutual 
inductance of arbitrarily oriented circular filaments via a generalisation of the Kalantarov-Zeitlin method. 


The function PPoletkin.m, refering to (22) of the same article mentioned above, calculates 
the mutual inductance between the primary circle and its projection on a tilting plane. 
The angular misalignment is given by angles theta and eta, while the
linear misalignment is defined by the coordinate zb of the point B crossing the tilted plane and the Z-axis.

Four examples corresponding  to Example 19, 20, 22 and 23 of the article show how the developed 
formulas and functions above can be used. 

The functions can be freely distributed, under the condition that the original work is cited.