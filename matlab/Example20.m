%Example of apllication of the developed formular

clear all;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameters for calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Example 20 in article (in terms of paper notations)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computation of mutual inductance 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Babic's example 11, page 3596

Rp=0.4;
Rs=0.1;
xb=0;
yb=0.2;
zb=0.1;
theta=90*pi/180;
eta=0*pi/180;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Grover's notation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 x1=0.2;
 x2=0;
 h=x1-x2*cos(theta);
 d=x2*sin(theta);
 psi=eta;

P=Poletkin(Rp,Rs,xb,yb,zb,theta,eta);


disp('----------------------------------------------------------');
disp('Parameters for  formula (20):');
disp(' ');
disp(['  Rp=' num2str(Rp) '      (i.e. parameter "A" of Grover)']);
disp(['  Rs=' num2str(Rs) '      (i.e. parameter "a" of Grover)']);
disp(['  xb=' num2str(xb) '      ==> parameter "rho" of Grover = sqrt(xc^2+yc^2)']);
disp(['  yb=' num2str(yb)]);
disp(['  zb=' num2str(zb) '      (i.e. parameter "d" of Grover)']);
disp(['  theta=' num2str(theta)       ]);
disp(['  eta=' num2str(eta)     '      (i.e. angle "psi" of Grover)'  ]); 
disp(' ');
disp(['   M=' num2str(P*1e9) ' nH']);
disp(' ');










% Preliminary computations
n_pts=701;
pc=[xb yb zb];        




figure(1)
clf
hold on
for tt=0:2*pi/n_pts:2*pi*(1-1/n_pts),
   pp=Rp*[cos(tt) sin(tt) 0];
   plot3(pp(1),pp(2),pp(3),'.b');
   
   if theta==pi/2 || theta==-pi/2, 
       psx=Rs.*cos(tt).*cos(eta);
       psy=Rs.*cos(tt).*sin(eta);
         psz= (pc(3)+Rs*sin(tt));
        ps=[pc(1)+psx pc(2)+psy psz];
        plot3(ps(1),ps(2),ps(3),'.g');
   else  
   rs=cos(theta)./sqrt((sin(tt-eta)).^2+(cos(theta).*cos(tt-eta)).^2);
      psx=Rs.*rs.*cos(tt);
   psy=Rs.*rs.*sin(tt);
   psz= (pc(3)+Rs.*rs.*sin(tt-eta).*tan(theta));
   ps=[pc(1)+psx pc(2)+psy psz];
   plot3(ps(1),ps(2),ps(3),'.g');
   end;
end



box on
set(gca,'DataAspectRatioMode','Manual');
title('Arrangement of the circles');
line([0 2*Rp],[0 0],[0 0]);
line([0 0],[0 3*Rp],[0 0]);
line([0 0],[0 0],[0 2*Rp]);
xlabel('x'); ylabel('y'); zlabel('z');
%axis(Rp*[-2 2 -2 3 -1 2]);
view([1 0.7 0.7]);





