%Example of apllication of the developed formular

clear all;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameters for calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Example 22 of paper (in terms of paper notations)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computation of mutual inductance 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Singularity. Rotating the circular filament tilted on 90 degrees 
%

Rp=0.4;
Rs=0.1;
xb=0.1;
yb=0.1;
zb=0.0;
theta=90.0000*pi/180;
eta=0*pi/180;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Grover's notation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 x1=zb;
 x2=yb;
 h=x1-x2*cos(theta);
 d=x2*sin(theta);
 psi=eta;

P=Poletkin(Rp,Rs,xb,yb,zb,theta,eta)

disp('----------------------------------------------------------');
disp('Parameters for formula (20):');
disp(' ');
disp(['  Rp=' num2str(Rp) '      (i.e. parameter "A" of Grover)']);
disp(['  Rs=' num2str(Rs) '      (i.e. parameter "a" of Grover)']);
disp(['  xb=' num2str(xb) '      ==> parameter "rho" of Grover = sqrt(xc^2+yc^2)']);
disp(['  yb=' num2str(yb)]);
disp(['  zb=' num2str(zb) '      (i.e. parameter "d" of Grover)']);
disp(['  theta=' num2str(theta)       ]);
disp(['  eta=' num2str(eta)     '      (i.e. angle "psi" of Grover)'   ]);
disp(' ');
disp(['   M=' num2str(P*1e9) ' nH']);
disp(' ');




% Conversion of above notation in Grover's notation
A=Rp; a=Rs; rho=d; d=h;



% Conversion of above notation in Babic's notation
Rp=A; Rs=a;
xc=0; yc=rho; zc=d;
a=sin(psi)*sin(theta);
b=-cos(psi)*sin(theta);
c=cos(theta);


M_Babic=Babic_24(Rp,Rs,[xc yc zc],[a b c]);
disp('----------------------------------------------------------');
disp('Parameters for Babic''s formula:');
disp(' ');
disp(['  Rp=' num2str(Rp) '      (i.e. parameter "A" of Grover)']);
disp(['  Rs=' num2str(Rs) '      (i.e. parameter "a" of Grover)']);
disp(['  xc=' num2str(xc) '      ==> parameter "rho" of Grover = sqrt(xc^2+yc^2)']);
disp(['  yc=' num2str(yc)]);
disp(['  zc=' num2str(zc) '      (i.e. parameter "d" of Grover)']);
disp(['   a=' num2str(a)]);
disp(['   b=' num2str(b)]);
disp(['   c=' num2str(c)]);
disp(' ');
disp('Result obtained with Babic''s formula (24):');
disp(' ');
disp(['   M_Babic=' num2str(M_Babic*1e9) ' nH']);
disp(' ');




% Preliminary computations
n_pts=701;
pc=[xb yb zb];        




figure(1)
clf
hold on
for tt=0:2*pi/n_pts:2*pi*(1-1/n_pts),
   pp=Rp*[cos(tt) sin(tt) 0];
   plot3(pp(1),pp(2),pp(3),'.b');
   
   if theta==pi/2 || theta==-pi/2, 
       psx=Rs.*cos(tt).*cos(eta);
       psy=Rs.*cos(tt).*sin(eta);
         psz= (pc(3)+Rs*sin(tt));
        ps=[pc(1)+psx pc(2)+psy psz];
        plot3(ps(1),ps(2),ps(3),'.g');
   else  
   rs=cos(theta)./sqrt((sin(tt-eta)).^2+(cos(theta).*cos(tt-eta)).^2);
      psx=Rs.*rs.*cos(tt);
   psy=Rs.*rs.*sin(tt);
   psz= (pc(3)+Rs.*rs.*sin(tt-eta).*tan(theta));
   ps=[pc(1)+psx pc(2)+psy psz];
   plot3(ps(1),ps(2),ps(3),'.g');
   end;
end



box on
set(gca,'DataAspectRatioMode','Manual');
title('Arrangement of the circles');
line([0 2*Rp],[0 0],[0 0]);
line([0 0],[0 3*Rp],[0 0]);
line([0 0],[0 0],[0 2*Rp]);
xlabel('x'); ylabel('y'); zlabel('z');
%axis(Rp*[-2 2 -2 3 -1 2]);
view([1 0.7 0.7]);


figure(2)
n=1;


% tester en prenant n/Rs

for eta=0:0.01*pi/6:pi*2;
a=sin(eta)*sin(theta);
b=-cos(eta)*sin(theta);
  
  M_B(n)=Babic_24(Rp,Rs,[xc yc zc],[a b c]);
  %M_B_mat(n)=Babic_24_int_matlab(Rp,Rs,[xc yc zc],[a b c]);
  
MP(n)=Poletkin(Rp,Rs,xb,yb,zb,theta,eta);
ang(n)=eta*180/pi;
n=n+1;
end;

% plot(ang,MP*1e9,'--gs',...
%     'LineWidth',3,...
%     'MarkerSize',10,...
%     'MarkerEdgeColor','b',...
%     'MarkerFaceColor',[0.5,0.5,0.5])

plot(ang,MP*1e9,'-k',...
    'LineWidth',3);
title('Distribution of the error of the developed formula in dependent on changing the eta-angle');
ylabel('M, nH');
xlabel('eta, degrees');


axis([0.1, 360, -1e-14, 1e-14 ]);
figure(3)
plot(ang,M_B*1e9,'-k',...
    'LineWidth',3);
title('Distribution of the error of the Babic formula in dependent on changing the eta-angle');
axis([0.1, 360, -1e-14, 3e-14 ]);
ylabel('M, nH');
xlabel('eta, degrees');
%for xb=yb=0.1
%axis([0.1, 360, 2.9195e-15, 2.9205e-15]) %for origin
% figure(4)
% plot(ang,M_B_mat*1e9,'-r',...
%     'LineWidth',3)

