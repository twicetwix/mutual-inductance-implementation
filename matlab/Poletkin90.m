%Summary of this function goes here
%   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Returns the mutual inductance between two circular filaments of radius Rp and Rs,
%which are mutually perpendicura to each other. Their centers are separated
%by coordinates xb,yb,zb, and angular position
% defined by the theta =90� and the angule eta (rotating angle around
% vertical axis). (Angular misalignment is the same as given by Gover).

% All dimensions must be in "meters" and angles in "radians"
%

%
% The units have been adapted to the S.I. system
%
% Programmed by Kirill Poletkin

function M = Poletkin90(Rp,Rs,xb,yb,zb,eta,tol)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin==6,  tol=1e-14;
elseif nargin<6 || nargin >7,
   error('Wrong number of parameters in function call (Poletkin90.m)!');
end

a=Rs/Rp;
dx=xb/Rs;
dy=yb/Rs;
dz=zb/Rs;

val1 = 4e-7*sqrt(Rs*Rp)*integral(@(r)dL(r,a,dx,dy,dz,eta,1),1,-1,'RelTol',0,'AbsTol',tol)
val2 = 4e-7*sqrt(Rs*Rp)*integral(@(r)dL(r,a,dx,dy,dz,eta,-1),-1,1,'RelTol',0,'AbsTol',tol)
M=+val1 + val2
;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%







%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Integrand function  
function f=dL(r,a,x,y,z,et,sg)
 % r is the length of integration
 % a is the ratio of Rs/Rp
 % th is the theta and et is the eta
 % sg is the sign for the evaluation of the hight
  
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %%%%%%% Dimensionless functions
    % dimensionless radius
    s=sqrt(x.^2+y.^2); % dimensionless parametere S
    rho=sqrt(s.^2+2.*r.*(x.*cos(et)+y.*sin(et))+r.^2);
   if sg==1, l=z+sqrt(1-r.^2); end
    if sg==-1, l=z-sqrt(1-r.^2); end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
 kk=4.*a.*rho./((a.*rho+1).^2+a.^2.*l.^2); 
  k=sqrt(kk);
 
  [K,E]=ellipke(kk);    

  Psi=(1-0.5*kk).*K-E;
  
  t1=sin(et).*(x+r.*cos(et));
  t2=cos(et).*(y+r.*sin(et));
  
f=(t1-t2).*Psi./(k.*rho.^1.5);

