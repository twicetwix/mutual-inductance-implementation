%Example of apllication of the developed formular

clear all;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameters for calculation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Example 23 of article (in terms of paper notations)

%The calculation of the mutual inductance between the primary coil and 
%its projection on the tilted plane

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computation of mutual inductance 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%for a case when theta=0, it corresponds to Example 5-4, page 215 in Kalantarovís book 

Rp=0.1;
Rs=0.1;
xb=0;
yb=0;
zb=0.04;
theta=15*pi/180;
eta=0*pi/180;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Grover's notation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% x1=0;
 x2=0;
 h=zb;
 d=yb;
 psi=eta;



P=PPoletkin(Rp,zb,theta,eta);

disp('----------------------------------------------------------');
disp('Parameters of formula (22) for projection:');
disp(' ');
disp(['  Rp=' num2str(Rp) ]);
disp(['  Rs=' num2str(0) ]);
disp(['  xb=' num2str(0) ]);
disp(['  yb=' num2str(0)]);
disp(['  zb=' num2str(zb) ]);
disp(['  theta=' num2str(theta)       ]);
disp(['  eta=' num2str(eta)       ]);
disp(' ');
disp(['   M=' num2str(P*1e9) ' nH']);
disp(' ');








% Preliminary computations
n_pts=701;
pc=[xb yb zb];        






figure(1)
clf
hold on
for tt=0:2*pi/n_pts:2*pi*(1-1/n_pts),
   pp=Rp*[cos(tt) sin(tt) 0];
   plot3(pp(1),pp(2),pp(3),'.b');
      
   psx=Rp.*cos(tt);
   psy=Rp.*sin(tt);
   psz= (pc(3)+Rp.*sin(tt-eta).*tan(theta));
   ps=[psx psy psz];
   plot3(ps(1),ps(2),ps(3),'.g');
end



box on
set(gca,'DataAspectRatioMode','Manual');
line([0 2*Rp],[0 0],[0 0]);
line([0 0],[0 3*Rp],[0 0]);
line([0 0],[0 0],[0 2*Rp]);
title('Projection of primary coil on a tilted plane')
xlabel('x'); ylabel('y'); zlabel('z');
%axis(Rp*[-2 2 -2 3 -1 2]);
view([1 0.7 0.7]);


