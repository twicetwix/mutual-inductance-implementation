%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Detailed explanation goes here
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function returns the mutual inductance between two circular filaments of radius Rp and Rs,
% whose centers are separated by coordinates xb,yb,zb, and angular position
% defined by the angule theta (tilting angle defined in an interval of 0<=theta<=360 degrees) 
%and eta (rotating angle around  vertical axis  defined in an interval of  0<=theta<=360). 
%The angular misalignment is defined in the same way as given by  Gover's notation).

% All dimensions must be in "meters" and angles in "radians"
%

% The units have been adapted to the S.I. system
%
% Programmed by Kirill Poletkin
%%
function M = Poletkin(Rp,Rs,xb,yb,zb,theta,eta,tol)




if nargin==7,  tol=1e-14;
elseif nargin<7 || nargin >8,
   error('Wrong number of parameters in function call (Poletkin.m)!');
end

 if theta==pi/2 ||  theta==-pi/2 
     M0=Poletkin90(Rp,Rs,xb,yb,zb,eta,tol); % Treatment of special case when Theta=90 degrees
 else
     a=Rs/Rp;
    dx=xb/Rs;
    dy=yb/Rs;
    dz=zb/Rs;
     M0=4e-7*sqrt(Rs*Rp)*integral(@(p)dL(p,a,dx,dy,dz,theta,eta),0,2*pi,'RelTol',0,'AbsTol',tol);
 end
 M=M0;
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%







%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Integrand function  
function f=dL(p,a,x,y,z,th,et)
 % p is the angle of integration
 % a is the ratio of Rs/Rp
 % th is the theta and et is the eta
  
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %%%%%%% Dimensionless functions
    r=cos(th)./sqrt((sin(p-et)).^2+(cos(th).*cos(p-et)).^2); % dimensionless radius
    s=sqrt(x.^2+y.^2); % dimensionless parametere S
    rho=sqrt(r.^2+2.*r.*(x.*cos(p)+y.*sin(p))+s.^2);
    l=z+r.*sin(p-et).*tan(th);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
 kk=4.*a.*rho./((a.*rho+1).^2+a.^2.*l.^2); 
  k=sqrt(kk);
 
  [K,E]=ellipke(kk);    

  Psi=(1-0.5*kk).*K-E;
  
  t1=x+0.5*r.^2.*(tan(th)).^2.*sin(2.*(p-et)).*y;
  t2=y-0.5*r.^2.*(tan(th)).^2.*sin(2.*(p-et)).*x;
  
f=(r+t1.*cos(p)+t2.*sin(p)).*r.*Psi./(k.*rho.^1.5);

