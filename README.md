# Mutual Inductance Implementation

Python 3 implementation of formula from [science article (Kirill Poletkin, Jan Gerrit Korvink)](https://www.researchgate.net/publication/331861230_Efficient_calculation_of_the_mutual_inductance_of_arbitrarily_oriented_circular_filaments_via_a_generalisation_of_the_Kalantarov-Zeitlin_method) for Electromechanical Systems course in <a href="https://innopolis.university/">Innopolis University</a>.

## Why that project:
There is an implementation provided by Kirill Poletkin in matlab. 
Matlab is very powerful and convenient tool to work with such tasks and formulas. 
However, to actually use matlab, you have to have a license, also matlab is not very "enterprise ready" solution.
For example, matlab code cannot be embedded in web server so easily as python. This project code cab be easily tasken and used in modern frameworks like fast-api, django and flask in enterprise.
Also students of Innopolis University can not use matlab freely: they need either to buy it, crack it or to use some other options.
This project demonstrates that even such complicated projects can be implemented in python, because python itself is very powerful.

## Implementation details and differences:
Python 3 is required (project was written using python 3.9). There was taken 5 examples from the article (19-23). 
Matlab implementation use 4 (without 21). Examples are represented as unit tests, having 100% coverage of the code. 
The main function names (`Poletkin`, `Poletkin90`, and `PPoletkin`) remains the same. 
There is used 5 external libraries: `numpy`, `scipy`, `mpmath` for calculations, `coverage` for coverage calculation in Pycharm and `matplotlib` for plot rendering. 
You can find matlab implementation in matlab folder inside repository.
There are some small modifications like avoiding zero division, which is accepted in matlab but not accepted in python.

## Changed plot building
I have added projection of second coil to first. Also I visualize angle between their centers. I am not using for loop like in matlab, I use numpy instead
![](imgs/plot19.png)
Important note is that the axis ratio is NOT 1:1:1 so coil can look invalid.
Unfortunately, there is no implementation in matplotlib for 3d plots yet for ratio config.

## About precision 
I have almost correct precision comparing to paper. 
I have used integration precision 50 in mpmath.
As I understood, in paper results are rounded on 4 places. (Like in example 23 there is: my result is `153.32329755577115` but in paper there is `153.3233`. So I have changed unit test precision in `assertAlmostEqual` method)

## Installation guide
Clone project using SSH
```
git clone git@gitlab.com:twicetwix/mutual-inductance-implementation.git
```
Or clone via HTTP
```
git clone https://gitlab.com/twicetwix/mutual-inductance-implementation.git
```
Install all dependencies. (It is highly recommended to use virtualenv)
```
pip install -r requirements.txt
```
You can try it as console application
```
python main.py
```
or run tests to show all examples
```
python -m unittest examples.py --show-plots
```
You need to close plots to check next result if you are not in pycharm configuration

